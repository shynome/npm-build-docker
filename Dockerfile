FROM plugins/base:amd64

RUN set -x \
  && apk add --no-cache --repository http://dl-3.alpinelinux.org/alpine/edge/main nodejs npm \
  && npm i -g npm

COPY npm-build.tgz /tmp/npm-build.tgz
RUN npm i -g /tmp/npm-build.tgz \
  && rm -rf /tmp/npm-build.tgz $(npm config get cache)

CMD [ "npm-build" ]
