
const fs = require('fs')
const shell = require('shelljs')
const { env } = require('@shynome/drone-utils')
const last_install_package_filepath = 'node_modules/last_install_package.json'
const current_package_filepath = 'package.json'

/**
 * @typedef { { [key:string]:string } } Deps
 * @typedef { { dependencies:Deps, devDependencies:Deps } } package_json
 */


/**
 * 
 * @param { package_json } json 
 */
function get_npm_deps_from_json (json) {
  let deps = Object.assign( {}, json.dependencies, json.devDependencies )
  let sorted_deps = Object.keys(deps).sort().reduce((t,k)=>( t[k]= deps[k], t ), /**@type {Deps}*/({}))
  return sorted_deps
}

/**@param {string} path */
function get_npm_deps_from_path (path) {
  let json = /**@type {*}*/({})
  if(fs.existsSync(path)){
    json = JSON.parse(fs.readFileSync(path,'utf8'))
  }
  return get_npm_deps_from_json(json)
}

exports.install = function () {

  let [ now_deps, last_deps ] = [ current_package_filepath, last_install_package_filepath ].map(get_npm_deps_from_path).map(c=>JSON.stringify(c))

  if( last_deps === now_deps){
    shell.echo(`依赖无变更.`)
    return
  }

  shell.exec(env.iget('install_cmd') || 'npm i')

  shell.exec(`cp ${current_package_filepath} ${last_install_package_filepath}`)
  
}
