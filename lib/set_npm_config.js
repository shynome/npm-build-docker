
const regx = /^(npm_|PLUGIN_NPM_)(.+)/i
const shell = require('shelljs')

/**@param {string} name */
const get_npm_key = (name)=>{
  let match = name.match(regx)
  return match?match[1]:null
}

const set_npm_config = ()=>{
  Object.keys(process.env).map(name=>{
    let npm_key = get_npm_key(name)
    if(!npm_key)return
    shell.exec(`npm config set ${npm_key} ${process.env[npm_key] || ''}`)
  })
}

module.exports = {
  regx,
  get_npm_key,
  set_npm_config,
}