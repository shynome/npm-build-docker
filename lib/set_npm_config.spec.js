const assert = require('assert')
const { regx } = require('./set_npm_config')
const cache = 'cache'

for(let prefix of [ 'npm', 'NPM', 'PLUGIN_NPM', ].map(s=>s+'_')){

  let str = prefix + cache
  
  let matched = str.match(regx)

  assert(
    matched!==null,
    str
  )

  let expected = [prefix,cache]
  assert.deepStrictEqual(
    matched.slice(1),
    expected,
    JSON.stringify([matched,expected],null,2),
  )
  
}
