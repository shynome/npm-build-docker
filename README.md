### Usage

```sh
docker run --rm -ti \
  # 如果有额外的 npm config 要设置, 需要设置这个值
  -e npm_config=1 \
  # npm cache dir 的默认值
  -e npm_cache=/drone/npm-cache \
  # npm install 的默认值
  -e npm_install='npm i' \
  shynome/npm-build
```